﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace myFinance.Controllers
{
    public class RegistrationLoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        } 
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Models.myFinanceUserModel user)
        {

            if (ModelState.IsValid)
            {
                using (var db = new myFinanceDatabaseEntities())
                {

                    var usercontainer = db.myFinanceUsers.Create();

                    usercontainer.Email = user.Email;
                    usercontainer.Nume = user.Nume;
                    usercontainer.Prenume = user.Prenume;
                    usercontainer.Parola = user.Parola;
                    usercontainer.ConfirmaParola = user.ConfirmaParola;
                    if (user.Parola == user.ConfirmaParola)
                    {
                        //se verifica daca mai exista un email introdus in baza de date
                        var count = db.myFinanceUsers.Count(u => u.Email == user.Email);
                        if (count == 0)
                        {
                            //se adauga userul in DB
                            db.myFinanceUsers.Add(usercontainer);
                            db.SaveChanges();

                            ViewBag.Successful = user.Email +
                                                 " te-ai inregistrat cu succes. Daca doresti sa accesezi site-ul Logheaza-te.";
                            return RedirectToActionPermanent("Index");
                        }
                        {

                            ViewBag.EroareEmail = "Adresa de email introdusa: " + user.Email +
                                                  " exista deja in baza de date. Inregistrati-va cu alt user";
                            return View("Register");
                        }
                    }
                    {
                        ViewBag.EroareParole = "Campurile: Parola si confirma parola trebuie sa fie identice. ";
                        return View("Register");
                    }
                }
            }
            {
                ModelState.AddModelError("", "Inregistrarea nu s-a putut realiza.");
            }
            return View();
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(Models.myFinanceUserModel user)
        {
            
                using (var db = new myFinanceDatabaseEntities())
                {
                    var admin =
                        db.myFinanceUsers.FirstOrDefault(
                            u => u.Email == user.Email && u.Parola == user.Parola && u.RoluriUser == "Administrator");
                    if (admin != null)
                    {
                    Session["Logat"] = "Logat cu email: " + user.Email;
                    return RedirectToAction("Index", "Administrator");}
                {  

                    var usr = db.myFinanceUsers.FirstOrDefault(u => u.Email == user.Email && u.Parola == user.Parola);
                    if (usr != null)
                    {
                        Session["Logat"] = "Logat cu email: " + user.Email;
                        return RedirectToAction("Index", "Logat");
                    }
                    {
                        ViewBag.EroareLogare = "Datele introduse nu sunt corecte. Logarea nu s-a produs. Va rugam introduceti datele din nou!";
                        return View();
                    }
                }
            }
            } 

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index");
        } 
    }
}