﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace myFinance.Models
{
    public class myFinanceUserModel
    {
        [Key]
        [EmailAddress]
        [StringLength(150)]
        [Display(Name = "Email : ")]
        [Required(ErrorMessage = "Email is required.")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Introduceti numele.")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Introduceti intre 6 si 20 de caractere.")]
        [Display(Name = "Nume : ")]
        public string Nume { get; set; }

        [Required(ErrorMessage = "Introduceti prenumele.")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Introduceti intre 6 si 20 de caractere.")]
        [Display(Name = "Prenume : ")]
        public string Prenume { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Introduceti intre 6 si 20 de caractere.")]
        [Display(Name = "Parola: ")]
        public string Parola { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6,ErrorMessage = "Introduceti intre 6 si 20 de caractere.")]
        [Display(Name = "Confirma parola: ")]
        public string ConfirmaParola { get; set; }

    }
}